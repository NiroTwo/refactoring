package mvc_pattern;

	public class main {
	    public static void main(String[] args) {
	        model model = new model();
	        view view = new view(model);
	        IOSpecialist io = new IOSpecialist();
	        Controller controller = new Controller(model, view);
	        

	        
	        controller.run();
	    }
	}