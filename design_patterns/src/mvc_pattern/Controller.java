package mvc_pattern;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {
	private model model;
	private view view;

	public Controller(model model, view view) {
		this.model = model;
		this.view = view;

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == view.getPlayButton()) {

		}
	}

	public void run() {

	}
}
