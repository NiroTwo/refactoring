package mvc_pattern;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class view {
    private JFrame frame;
    private model model;
    private JLabel welcomeLabel;
    private JLabel playerNamePromptLabel;
    private JTextField playerNameTextField;
    private JButton playButton;


    public view(model model) {
        this.model = model;
        frame = new JFrame("Abominodo - The Best Dominoes Puzzle Game in the Universe");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        playerNamePromptLabel = new JLabel("Welcome! Have a good time playing Abominodo");
        playerNamePromptLabel.setHorizontalAlignment(SwingConstants.CENTER);
        playerNamePromptLabel.setBounds(146, 393, 533, 46);
        playerNamePromptLabel.setFont(new Font("Script MT Bold", Font.BOLD, 22));
        playerNamePromptLabel.setForeground(new Color(153, 0, 51));



        // Create a JPanel to hold the welcome label
        JPanel welcomePanel = new JPanel();
        
        // Create a JPanel to hold the player name prompt and text field
        JPanel playerNamePanel = new JPanel();
        playerNamePanel.setBackground(new Color(255, 102, 153));
        playerNamePanel.setLayout(null);
        playerNameTextField = new JTextField();
        playerNameTextField.setBackground(new Color(255, 255, 255));
        playerNameTextField.setBounds(247, 214, 134, 32);
        playerNameTextField.setDropMode(DropMode.INSERT);
        playerNameTextField.setHorizontalAlignment(SwingConstants.CENTER);
        playerNamePanel.add(playerNameTextField);
        playerNamePanel.add(playerNamePromptLabel);

        // Create a JPanel to hold the main menu buttons
        JPanel mainMenuPanel = new JPanel();


        // Add the panels to the frame
        frame.getContentPane().add(welcomePanel, BorderLayout.NORTH);
        frame.getContentPane().add(playerNamePanel, BorderLayout.CENTER);
        
        JLabel lblNewLabel = new JLabel("Enter Your Name To Continue");
        lblNewLabel.setForeground(new Color(255, 255, 255));
        lblNewLabel.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel.setBounds(20, 212, 217, 32);
        playerNamePanel.add(lblNewLabel);
        
        JButton btnNewButton = new JButton("Confirm");
        btnNewButton.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
        btnNewButton.setForeground(new Color(153, 0, 0));
        btnNewButton.setBackground(new Color(255, 255, 255));
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        	}
        });
        btnNewButton.setBounds(544, 208, 112, 32);
        playerNamePanel.add(btnNewButton);
        
                welcomeLabel = new JLabel("Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe");
                welcomeLabel.setBounds(42, 94, 734, 29);
                playerNamePanel.add(welcomeLabel);
                welcomeLabel.setForeground(new Color(0, 0, 0));
                welcomeLabel.setFont(new Font("Franklin Gothic Demi Cond", Font.BOLD, 23));
                playButton = new JButton("Play");
                playButton.setBounds(374, 449, 81, 46);
                playerNamePanel.add(playButton);
                playButton.setBackground(new Color(153, 51, 0));
                playButton.setForeground(new Color(255, 255, 255));
                playButton.setFont(new Font("Gill Sans Ultra Bold", Font.PLAIN, 13));
        frame.getContentPane().add(mainMenuPanel, BorderLayout.SOUTH);

        frame.setVisible(true);
    }

    public void displayGoodbyeMessage() {
        JOptionPane.showMessageDialog(frame, "It is a shame that you did not want to play", "Goodbye", JOptionPane.INFORMATION_MESSAGE);
    }

    public void setModel(model model) {
        this.model = model;
    }

    public String getPlayerName() {
        return playerNameTextField.getText();
    }

	public Component getMainPanel() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object getPlayButton() {
		// TODO Auto-generated method stub
		return null;
	}




}
